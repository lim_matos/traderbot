
'use strict';
let assert = require('assert');
let Helper = codecept_helper;
let XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const sql = require('mssql');
var config = 'mssql://cinq:c1nqs3rv1c0@177.71.197.88/NegocieCoins_Teste';

class main_Helper extends Helper {

  compareNotEqual(var1, var2, mensagem) {
    if (var1 !== var2) {
      assert.notEqual(var1, var2, mensagem);
      return;
    }
    assert.fail(mensagem);
  }

  /*
    Moeda:  
            'B'-bitCoin
            'L'-LightCoin
    Valor:  
            'V'-Venda
            'C'-Compra
            'U'-Ultimo Valor Negociado
            'A'-Valor mais alto
            'B'-Valor mais baixo
            'VOL'-Volume
    Retorno:
            Decimal separado por '.' ex '36000.0100'
  */
  getValorMoedaApi(Moeda, Valor) {
    var url = "https://broker.negociecoins.com.br/api/v3/";
    var retornoAPI = '';
    if (Moeda = "B") {
      url = url + "btcbrl";
      console.log('Recuperando valor BTC-BRL');
    } else {
      url = url + "ltcbrl"
      console.log('Recuperando valor LTC-BRL');
    }
    url = url + "/ticker";
    var Httpreq = new XMLHttpRequest();
    Httpreq.open("GET", url, false);
    Httpreq.send(null);
    //console.log(Httpreq.responseText);
    retornoAPI = Httpreq.responseText;

    retornoAPI = retornoAPI.split(":");

    var resultado = "";
    switch (Valor) {
      case 'V'://Venda
        retornoAPI = retornoAPI[2].split(",");
        resultado = retornoAPI[0];
        console.log('Valor de Venda:' + resultado);
        break;
      case 'C'://compra
        retornoAPI = retornoAPI[3].split(",");
        resultado = retornoAPI[0];
        console.log('Valor de Compra:' + resultado);
        break;
      case 'U'://ultimo
        retornoAPI = retornoAPI[4].split(",");
        resultado = retornoAPI[0];
        console.log('Último Valor Negociado:' + resultado);
        break;
      case 'A'://alta
        retornoAPI = retornoAPI[5].split(",");
        resultado = retornoAPI[0];
        console.log('Valor mais alto:' + resultado);
        break;
      case 'B'://Baixa
        retornoAPI = retornoAPI[6].split(",");
        resultado = retornoAPI[0];
        console.log('Valor mais baixo:' + resultado);
        break;
      case 'VOL'://Volume
        retornoAPI = retornoAPI[7].split(",");
        retornoAPI = retornoAPI[0].split("}");
        resultado = retornoAPI[0];
        console.log('Volume:' + resultado);
        break;
      default:
        resultado = "00,00"
    }
    return resultado;
  }

  salvarCotacao(Exchange, Moeda) {
    var url = "";
    var result;

    if (Exchange == 'NEGOCIECOINS') {
      url = "https://broker.negociecoins.com.br/api/v3/" + Moeda + "/ticker";
    }
    if (Exchange == 'MERCADOBITCOIN') {
      url = "https://www.mercadobitcoin.net/api/" + Moeda + "/ticker/";
    }

    if (Exchange == 'FOXBIT') {
      url = "https://api.blinktrade.com/api/v1/BRL/ticker?crypto_currency=" + Moeda;
    }

    var Httpreq = new XMLHttpRequest();
    Httpreq.open("GET", url, false);
    Httpreq.send(null);

    if (Httpreq.responseText.length == 0)
      return false;

    if (Exchange == 'NEGOCIECOINS') {
      result = JSON.parse(Httpreq.responseText);
      result.exchange = Exchange;
      result.coin = Moeda;
      this.InserirCotacao(result);
    }
    if (Exchange == 'MERCADOBITCOIN') {
      result = JSON.parse(Httpreq.responseText);
      result = result.ticker;
      result.exchange = Exchange;
      result.coin = Moeda;
      this.InserirCotacao(result);
    }
    if (Exchange == 'FOXBIT') {
      result = JSON.parse(Httpreq.responseText);
      result.exchange = Exchange;
      result.coin = Moeda;
      result.date = Date.now() / 1000;
      console.log(result);
      this.InserirCotacao(result);
    }
  }

  InserirCotacao(Dados) {
    var cmdSql = 'insert into coinValues (exchange,coin,date,sell,buy,last,high,low,vol) ';

    cmdSql = cmdSql + 'values ('
      + '\'' + Dados.exchange + '\'' + ','
      + '\'' + Dados.coin + '\'' + ','
      + 'DATEADD("s",' + Dados.date + ',\'01/01/1970 00:00:00\'),'
      + parseFloat(Dados.sell) + ','
      + parseFloat(Dados.buy) + ','
      + parseFloat(Dados.last) + ','
      + parseFloat(Dados.high) + ','
      + parseFloat(Dados.low) + ','
      + parseFloat(Dados.vol)
      + ');';
    console.log('comando sql');
    console.log(cmdSql);


    var dbConn = new sql.ConnectionPool(config);
    //3.
    dbConn.connect().then(function () {
      //4.
      var transaction = new sql.Transaction(dbConn);
      //5.
      transaction.begin().then(function () {
        //6.
        var request = new sql.Request(transaction);
        //7.
        request.query(cmdSql)
          .then(function () {
            //8.
            transaction.commit().then(function (recordSet) {
              //console.log('recordset'+recordSet);
              dbConn.close();
            }).catch(function (err) {
              //9.
              console.log("Error in Transaction Commit " + err);
              dbConn.close();
            });
          }).catch(function (err) {
            //10.
            console.log("Error in Transaction Begin " + err);
            dbConn.close();
          });

      }).catch(function (err) {
        //11.
        console.log(err);
        dbConn.close();
      });
    }).catch(function (err) {
      //12.
      console.log(err);
    });

  }
  
  RecuperarCotacao(exchange) {

    var cmdSql = 'select top 1 * from dbo.coinValues where exchange =';
    var dadosRetorno = {};
    cmdSql = cmdSql + '\'' + exchange + '\' order by date desc';

    console.log('comando sql');
    console.log(cmdSql);

    sql.connect(config, function (err) {
      if (err) console.log(err);
      // create Request object
      var request = new sql.Request();
      // query to the database and get the records
      request.query(cmdSql, function (err, recordset) {
        dadosRetorno.exchange = recordset.recordset[0].exchange.trim();
        dadosRetorno.coin = recordset.recordset[0].coin.trim();
        dadosRetorno.date = recordset.recordset[0].date;
        dadosRetorno.sell = recordset.recordset[0].sell;
        dadosRetorno.buy = recordset.recordset[0].buy;
        dadosRetorno.last = recordset.recordset[0].last;
        dadosRetorno.high = recordset.recordset[0].high;
        dadosRetorno.low = recordset.recordset[0].low;
        dadosRetorno.vol = recordset.recordset[0].vol;
        sql.close();

        //finalizar tratamento
        //console.log('Dados retorno', typeof (dadosRetorno));
        //console.log(dadosRetorno);
        return Promise.all; //dadosRetorno;

      });
    });
  }
}

module.exports = main_Helper;
