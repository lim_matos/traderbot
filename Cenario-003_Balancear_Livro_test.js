/// <reference path="./steps.d.ts" />
/*jshint esversion: 6 */

Feature('Cenário-003 - Balancear Livro de Ofertas');

Scenario('Balancear livro de ofertas', function*(I){

    var porcentagem = 0.005; // porcentagem aceitavel para diferenca com o mercado - Salvar no banco
    
    //I.Login();
    //I.click('Livro de Ofertas');
    //I.salvarCotacao('FOXBIT','BTC');
    //I.wait(2);
    //I.salvarCotacao('NEGOCIECOINS','btcbrl');
    //I.wait(2);
    //I.salvarCotacao('MERCADOBITCOIN','BTC');
    //I.wait(2);

    //Calcular preco medio do mercado
    cotacaoFox        =  I.buscarCotacao('FOXBIT');
    I.say('Valor capturado FOX: ' +cotacaoFox);
    I.wait(3);
    cotacaoNegocie    = I.buscarCotacao('NEGOCIECOINS');
    I.wait(3);
    cotacaoMercadoBtc = I.buscarCotacao('MERCADOBITCOIN');
    I.wait(3);
        
    I.say('Valor capturado NEG: ' +cotacaoNegocie.last);
    I.say('Valor capturado MBTC: '+cotacaoMercadoBtc.last);
    
    var precoMercado = ((cotacaoFox.last + cotacaoNegocie.last + cotacaoMercadoBtc.last)/3);
    var parametroMercado = (precoMercado *  porcentagem);
    
    I.say('Preço Medio Mercado:'+precoMercado);
    I.say('Parametro:'          +parametroMercado);
    
    var ultimoPrecoVenda  = cotacaoNegocie.sell;   
    var ultimoPrecoCompra = cotacaoNegocie.buy;
        
    I.say('Ultima venda NEG:' +ultimoPrecoVenda);
    I.say('Ultima compra NEG:'+ultimoPrecoCompra);
    
    var diferencaVenda  = ultimoPrecoVenda - precoMercado;
    var diferencaCompra = ultimoPrecoCompra - precoMercado;
    var diferencaLivros = '';    //ultimoPrecoCompra - ultimoPrecoVenda;
    
    I.say('Diferenca venda NEG:' +diferencaVenda);
    I.say('Diferenca compra NEG:'+diferencaCompra);
    


 /*   
    while(diferencaCompra > parametroMercado  && diferencaVenda > parametroMercado ){ //Prices are higher than the market
        //EnviarOrdemVendaTopo();
        let valorOrdem = yield I.grabTextFrom('(//td[@aria-describedby="listVenda_price"])[1]');
        I.say('Valor capturado: '+valorOrdem);
    
        valorOrdem = valorOrdem.replace('.','');
        I.say('Valor sem ponto: '+valorOrdem);
        valorOrdem = valorOrdem.replace(',','.');
        I.say('Valor com ponto: '+valorOrdem);
    
        var valorOrdemFloat = parseFloat(valorOrdem);
        I.say('Valor float: '+valorOrdemFloat);
    
        valorOrdemFloat = valorOrdemFloat - 0.01;
        valorOrdemFloat = valorOrdemFloat.toFixed(2);
        I.say('Valor float: '+valorOrdemFloat);
    
        I.fillField('input[name*=TextBoxQuantidadeVenda]','0,005');
        I.fillField('input[name*=TextBoxPrecoVenda]',''+valorOrdemFloat);
        I.ExecutarVenda();
        I.waitForText('Ordem enviada com sucesso',10);

        //ComprarMinhaOrdem();

        I.fillField('input[name*=TextBoxQuantidadeCompra]','0,005');
        I.fillField('input[name*=TextBoxPrecoCompra]',''+valorOrdemFloat);
        I.ExecutarCompra();
        I.waitForText('Ordem enviada com sucesso',10);

        //calcular diferencas 
    }
    /*
    while(diferencaCompra < 100 && diferencaVenda < 100){ //Prices are lower than the market
        EnviarOrdemCompraTopo();
        VenderMinhaOrdem();
        //calcular diferencas
    } 
    */   
});

