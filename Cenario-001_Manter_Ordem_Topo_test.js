/// <reference path="./steps.d.ts" />
/*jshint esversion: 6 */


Feature('Cenário-001 - Manter Ordem no topo');

Scenario('Verificar Ordem no topo', function*(I) {
    var myUser = 'Mercedes_7081339'; 
 
    I.Login();
    I.click('Livro de Ofertas');

    //Ordem de compra no topo

    I.waitForText('Ordens de Compra',5);
    let userOrdemCompraTopo = yield I.grabTextFrom('//*[@id="1"]/td[@aria-describedby="listCompra_user"]'); 
    I.say('Usuário capturado:'+userOrdemCompraTopo);  
    I.wait(5);
    
    if( userOrdemCompraTopo !== myUser){                                                                         
        I.click('(//td[contains(.,"Mercedes_70813")]/../td[@aria-describedby="listCompra_action"])[1]');
        I.waitForText('Ordem cancelado com sucesso',10);
        I.see('Ordem cancelado com sucesso');
        
        let valorOrdem = yield I.grabTextFrom('(//td[@aria-describedby="listCompra_price"])[1]');
        I.say('Valor capturado: '+valorOrdem);
    
        valorOrdem = valorOrdem.replace('.','');
        I.say('Valor sem ponto: '+valorOrdem);
        valorOrdem = valorOrdem.replace(',','.');
        I.say('Valor com ponto: '+valorOrdem);
    
        var valorOrdemFloat = parseFloat(valorOrdem);
        I.say('Valor float: '+valorOrdemFloat);
    
        valorOrdemFloat = valorOrdemFloat + 0.01;
        valorOrdemFloat = valorOrdemFloat.toFixed(2);
        I.say('Valor float: '+valorOrdemFloat);
    
        I.fillField('input[name*=TextBoxQuantidadeCompra]','0,005');
        I.fillField('input[name*=TextBoxPrecoCompra]',''+valorOrdemFloat);
        I.ExecutarCompra();
        I.waitForText('Ordem enviada com sucesso',10);
    }
});


Scenario('Verificar Ordem de venda no topo', function*(I) {
    var myUser = 'Mercedes_7081339'; 
    var ultimaOrdemVenda = '';
 
    I.Login();
    I.click('Livro de Ofertas');
    I.waitForText('Ordens de Compra',5);
    let userOrdemVendaTopo = yield I.grabTextFrom('//*[@id="1"]/td[@aria-describedby="listVenda_user"]');  //Gets the user of the first order in the book
    I.say('Usuário capturado:'+userOrdemVendaTopo);  
    I.wait(5);
    
    if( userOrdemVendaTopo !== myUser){                                                                         
        I.click('(//td[contains(.,"Mercedes_70813")]/../td[@aria-describedby="listVenda_action"])[1]');
        I.waitForText('Ordem cancelado com sucesso',5);
        I.see('Ordem cancelado com sucesso');
        
        let valorOrdem = yield I.grabTextFrom('(//td[@aria-describedby="listVenda_price"])[1]');
        I.say('Valor capturado: '+valorOrdem);
    
        valorOrdem = valorOrdem.replace('.','');
        I.say('Valor sem ponto: '+valorOrdem);
        valorOrdem = valorOrdem.replace(',','.');
        I.say('Valor com ponto: '+valorOrdem);
    
        var valorOrdemFloat = parseFloat(valorOrdem);
        I.say('Valor float: '+valorOrdemFloat);
    
        valorOrdemFloat = valorOrdemFloat - 0.01;
        valorOrdemFloat = valorOrdemFloat.toFixed(2);
        I.say('Valor float: '+valorOrdemFloat);
    
        I.fillField('input[name*=TextBoxQuantidadeVenda]','0,005');
        I.fillField('input[name*=TextBoxPrecoVenda]',''+valorOrdemFloat);
        I.ExecutarVenda();
        I.waitForText('Ordem enviada com sucesso',10);
             
    }
});
