
'use strict'; 
const newLocal = this;
// in this file you can append custom step methods to 'I' object


module.exports = function() {
    return actor( {
        
        // Define custom steps here, use 'this' to access default methods of I.
        // It is recommended to place a general 'login' function here.
        baseQueryString : '/#/login',
		
        pageLogin : {
            campoUsuario: 'input[placeholder="Usuário"]',
            campoSenha: 'input[placeholder="Senha"]',
			buttonEntrar:'input[value="ENTRAR"]'
        },
		
        listUsuario : [{usuario:'usuarioTeste',senha:'123456', pin:'7733'}],

        listContaBancaria: [{agencia: '1234', conta:'12345',agencia:'8'}],
        
        numeroAleatorio:'', //utilize esta variavel apos gerar numeros aleatorios

        Login : function() {
            var User = this.listUsuario[0].usuario;
            var Pass = this.listUsuario[0].senha;
            this.amOnPage('#/login');
            this.fillField(this.pageLogin.campoUsuario, User);
            this.fillField(this.pageLogin.campoSenha, Pass);
            this.click(this.pageLogin.buttonEntrar);
            this.waitForText('Painel de Controle', 30);
            this.see('Painel de Controle')
        },
        ExecutarVenda : function() {
            var Pin = this.listUsuario[0].pin;
            this.click('a[id=aEnviarVenda]');
            this.waitForText('Confirme os dados da ordem de venda!',5)
            this.see('Confirme os dados da ordem de venda!');
            this.fillField('input[name*=TextBoxPINVenda]',Pin);
            this.click('input[name*=ButtonVender]');
        },
        ExecutarCompra : function() {
            var Pin = this.listUsuario[0].pin;
            this.click('a[id=aEnviarCompra]');
            this.waitForText('Confirme os dados da ordem de compra!',5)
            this.see('Confirme os dados da ordem de compra!');
            this.fillField('input[name*=TextBoxPINCompra]',Pin);
            this.click('input[name*=ButtonComprar]');
        },
        GerarNumero : function(tamanho) {
            var retorno='';
            for (let index = 0; index < parseFloat(tamanho); index++) {
                retorno = retorno +''+Math.floor(Math.random() * 10)  ;
            }
            this.numeroAleatorio = retorno;
        },

        CadastrarContaBancaria : function(Agencia, Conta, Digito) {
            var Pin = this.listUsuario[0].pin;
            within({frame:'iframe[id*=TB_iframeContent]'}, () => {                                                       
                this.waitForText('Cadastro de Conta Bancária',10);
                this.selectOption('select[name*=DropDownListBanco]', '003 - Banco da Amazônia S.A.');
                this.fillField('input[id*="TextBoxAgencia"]', Agencia);
                this.fillField('input[id*="TextBoxConta"]', Conta);
                this.fillField('input[id*="TextBoxDigito"]', Digito);
                this.fillField('input[id*="TextBoxPIN"]', Pin);
                this.click('input[id*="ButtonSalvar"]');
            });
        },
        
        buscarCotacao : async function (exchange){
            const response = await this.RecuperarCotacao(exchange);
            const cotacao = await response.json();
            console.log(cotacao);
            return cotacao;
        },
    });
}