/// <reference path="./steps.d.ts" />
/*jshint esversion: 6 */


Feature('Cenário-002 - Buscar precos Mercado');

var ultimoPrecoFoxBit = '';
var ultimoPrecoNegocie = '';
var ultimoPrecoMercadoBitCoin = '';

Scenario('Buscar precos Mercado', function*(I){
        I.Login();
        I.openNewTab();
        I.amOnPage('http://www.icoinomia.com.br/?Moeda=BTCBRL');
        I.wait(30);
        ultimoPrecoFoxBit = yield I.grabTextFrom('((//span[contains(.,"Foxbit")])/../..//td[@data-label="ÚLTIMO(R$)"])');
        ultimoPrecoNegocie = yield I.grabTextFrom('((//span[contains(.,"NegocieCoins")])/../..//td[@data-label="ÚLTIMO(R$)"])');
        ultimoPrecoMercadoBitCoin = yield I.grabTextFrom('((//span[contains(.,"MercadoBitcoin")])/../..//td[@data-label="ÚLTIMO(R$)"])')
        I.switchToPreviousTab();
        I.wait(5);
        I.say('Valor capturado FoxBit:' +ultimoPrecoFoxBit);
        I.say('Valor capturado Negocie:' +ultimoPrecoNegocie);
        I.say('Valor capturado Mercado BitCoin:' +ultimoPrecoMercadoBitCoin);
});

Scenario('Testar Busca', function*(I){
    I.say('Valor capturado FoxBit:' +ultimoPrecoFoxBit);
    I.say('Valor capturado Negocie:' +ultimoPrecoNegocie);
    I.say('Valor capturado Mercado BitCoin:' +ultimoPrecoMercadoBitCoin);
});

