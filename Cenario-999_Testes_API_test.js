/// <reference path="./steps.d.ts" />
/*jshint esversion: 6 */


Feature('Cenário-999 - Testes de Funções');
/*
Scenario('Teste chamada api', function*(I) {
    var temp='eita';
    temp = yield I.getValorMoedaApi('B','V');
    I.say('Venda:'+temp);
    temp = yield I.getValorMoedaApi('B','C');
    I.say('Compra:'+temp);
    temp = yield I.getValorMoedaApi('B','U');
    I.say('Ultimo:'+temp);
    temp = yield I.getValorMoedaApi('B','A');
    I.say('Alta:'+temp);
    temp = yield I.getValorMoedaApi('B','B');
    I.say('Baixa:'+temp);
    temp = yield I.getValorMoedaApi('B','VOL');
    I.say('Volume:'+temp);
    
});*/

Scenario('Teste chamada api', function*(I) {
   // I.salvarCotacao('FOXBIT','BTC');
   // I.wait(2);
   // I.RecuperarCotacao('FOXBIT');
    var ultimoPrecoFox     = I.RecuperarCotacao('FOXBIT');
    I.wait(2);
    var ultimoPrecoNegocie = I.RecuperarCotacao('NEGOCIECOINS');
    I.wait(2);
    var ultimoPrecoMercado = I.RecuperarCotacao('MERCADOBITCOIN');
    I.wait(2);
    
    I.say('Valor capturado FOX: '+ultimoPrecoFox);
    I.say('Valor capturado NEG: '+ultimoPrecoNegocie);
    I.say('Valor capturado MBTC: '+ultimoPrecoMercado);
});